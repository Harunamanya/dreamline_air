-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2019 at 02:44 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dreamairlinedb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tiket`
--

CREATE TABLE `tiket` (
  `id` int(11) NOT NULL,
  `asal` varchar(128) NOT NULL,
  `tujuan` varchar(128) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tiket`
--

INSERT INTO `tiket` (`id`, `asal`, `tujuan`, `price`) VALUES
(1, 'jakarta', 'bandung', 300000),
(2, 'bandung', 'jakarta', 300000),
(3, 'jakarta', 'bali', 500000),
(4, 'bali', 'jakarta', 500000),
(5, 'jakarta', 'medan', 550000),
(6, 'medan', 'jakarta', 550000),
(7, 'bandung', 'bali', 400000),
(8, 'bali', 'bandung', 400000),
(9, 'bandung', 'medan', 50000),
(10, 'medan', 'bandung', 500000),
(11, 'bali', 'medan', 600000),
(12, 'medan', 'bali', 600000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `kota_asal` varchar(256) NOT NULL,
  `kota_tujuan` varchar(256) NOT NULL,
  `tgl_go` date NOT NULL,
  `tgl_back` date NOT NULL,
  `jml_dewasa` int(2) NOT NULL,
  `jml_anak` int(2) NOT NULL,
  `user` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `kota_asal`, `kota_tujuan`, `tgl_go`, `tgl_back`, `jml_dewasa`, `jml_anak`, `user`) VALUES
(18, 'Jakarta, Soekarno Hatta International Airport', 'Bali, Ngurah Rai International Airport', '2019-12-25', '2019-12-27', 2, 1, 'teguh ramadhan');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(9, 'teguh ramadhan', 'teguhrmdhn@gmail.com', 'default.jpg', '$2y$10$kDjPgvJxNKBcnM2upHIlwexAg2u0NFwsen0z59kTPBe2F.uhqwOgC', 2, 1, 1576921340),
(10, 'admin', 'user@admin.com', 'default.jpg', '$2y$10$o51v76SR/lkH5j/sOuAV9eNMf7gQcVq9VSJnQ0tikpsPWo0JVLzyW', 1, 1, 1576922599),
(11, 'harun nurahman', 'harunnurahman@gmail.com', 'default.jpg', '$2y$10$OsI0a8f8uQVrZdaZp57t9.hEfLyhclhUeTUj8eqDS7F2u8Hj6RlbW', 2, 1, 1577201800),
(12, 'rizal anggara', 'rizalanggara@gmail.com', 'default.jpg', '$2y$10$Q79DP0OHWJBr5zUbPEAeEuEtSR.7W/xEAGp9.vD/erfphthsFOJxy', 2, 1, 1577202743);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'adminitrator'),
(2, 'member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tiket`
--
ALTER TABLE `tiket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
