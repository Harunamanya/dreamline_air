<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = 'Dream Airline | user page';
        $data['user'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();


        $this->load->view('template/header', $data);
        $this->load->view('user/index', $data);
        $this->load->view('template/footer');
    }

    public function get_tiket()
    {
        $datauser['user'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();

        $this->form_validation->set_rules('kota_asal', 'From', 'required|trim');
        $this->form_validation->set_rules('kota_tujuan', 'Destination', 'required|trim');

        if ($this->form_validation->run() == false) {
            $this->load->view('user');
        } else {
            $data = [
                'kota_asal' => ($this->input->post('kota_asal')),
                'kota_tujuan' => ($this->input->post('kota_tujuan')),
                'tgl_go' => ($this->input->post('tgl_go')),
                'tgl_back' => ($this->input->post('tgl_back')),
                'jml_dewasa' => ($this->input->post('jml_dewasa')),
                'jml_anak' => ($this->input->post('jml_anak')),
                'user' => ($datauser['user']['name'])
            ];

            $this->db->insert('transaksi', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Congratulation! your tickets has been created, please cek at <b><p>My Tickets!</p></b></div>');
            redirect('user');
        }
    }
}
