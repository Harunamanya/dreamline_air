<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Insurance extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {

        $data['title'] = 'Insurance';
        $data['user'] = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('template/header_landing', $data);
        $this->load->view('user/insurance');
        $this->load->view('template/footer_landing');
    }
}
