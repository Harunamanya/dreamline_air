			<!-- start banner Area -->
			<section class="banner-area relative">
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row fullscreen align-items-center justify-content-between">
						<div class="col-lg-6 col-md-6 banner-left">

						</div>
						<div class="col-lg-6 col-md-8 banner-right mt-5">
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="flight" role="tabpanel" aria-labelledby="flight-tab">
									<form class="form-wrap" action="<?php echo base_url() . 'user/get_tiket'; ?>" method="POST">
										<h5 class="mb-3">Flights</h5>

										<div class="form-group">
											<?= $this->session->flashdata('message'); ?>
											<select class="form-control" id="kota_asal" name="kota_asal">
												<option>From</option>
												<option>Jakarta, Soekarno Hatta International Airport</option>
												<option>Bandung. Husein Sastranegara Airport</option>
												<option>Bali, Ngurah Rai International Airport</option>
												<option>Medan, Kualanamu International Airport</option>
											</select>
										</div>
										<div class="form-group">
											<select class="form-control" id="kota_tujuan" name="kota_tujuan">
												<option>To</option>
												<option>Jakarta, Soekarno Hatta International Airport</option>
												<option>Bandung. Husein Sastranegara Airport</option>
												<option>Bali, Ngurah Rai International Airport</option>
												<option>Medan, Kualanamu International Airport</option>
											</select>
										</div>
										<input type="date" class="form-control date-picker" name="tgl_go" placeholder="Start " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Start '">
										<input type="date" class="form-control date-picker" name="tgl_back" placeholder="Return " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Return '">
										<input type="number" min="1" max="20" class="form-control" name="jml_dewasa" placeholder="Adults " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Adults '">
										<input type="number" min="1" max="20" class="form-control" name="jml_anak" placeholder="Child " onfocus="this.placeholder = ''" onblur="this.placeholder = 'Child '">
										<input type='submit' class="primary-btn text-uppercase" value="Create Tickets">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End banner Area -->



			<!-- Start testimonial Area -->
			<section class="testimonial-area section-gap">
				<div class="container">
					<!-- Start popular-destination Area -->
					<section class="popular-destination-area section-gap">
						<div class="container">
							<div class="row d-flex justify-content-center">
								<div class="menu-content pb-70 col-lg-8">
									<div class="title text-center">
										<h1 class="mb-10">Popular Destinations</h1>
										<p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day.</p>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4">
									<div class="single-destination relative">
										<div class="thumb relative">
											<div class="overlay overlay-bg"></div>
											<img class="img-fluid" src="<?= base_url('assets/') ?>img/d1.jpg" alt="">
										</div>
										<div class="desc">
											<a href="#" class="price-btn">$150</a>
											<h4>Mountain River</h4>
											<p>Paraguay</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="single-destination relative">
										<div class="thumb relative">
											<div class="overlay overlay-bg"></div>
											<img class="img-fluid" src="<?= base_url('assets/') ?>img/d2.jpg" alt="">
										</div>
										<div class="desc">
											<a href="#" class="price-btn">$250</a>
											<h4>Dream City</h4>
											<p>Paris</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="single-destination relative">
										<div class="thumb relative">
											<div class="overlay overlay-bg"></div>
											<img class="img-fluid" src="<?= base_url('assets/') ?>img/d3.jpg" alt="">
										</div>
										<div class="desc">
											<a href="#" class="price-btn">$350</a>
											<h4>Cloud Mountain</h4>
											<p>Sri Lanka</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End popular-destination Area -->
					<div class="row d-flex justify-content-center">
						<div class="menu-content pb-70 col-lg-8">
							<div class="title text-center">
								<h1 class="mb-10">Testimonial from our Clients</h1>
								<p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall from </p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="active-testimonial">
							<div class="single-testimonial item d-flex flex-row">
								<div class="thumb">
									<img class="img-fluid" src="<?= base_url('assets/') ?>img/elements/user1.png" alt="">
								</div>
								<div class="desc">
									<p>
										Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills, the bigger the payoff you.
									</p>
									<h4>Harriet Maxwell</h4>
									<div class="star">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
							<div class="single-testimonial item d-flex flex-row">
								<div class="thumb">
									<img class="img-fluid" src="<?= base_url('assets/') ?>img/elements/user2.png" alt="">
								</div>
								<div class="desc">
									<p>
										A purpose is the eternal condition for success. Every former smoker can tell you just how hard it is to stop smoking cigarettes. However.
									</p>
									<h4>Carolyn Craig</h4>
									<div class="star">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
							<div class="single-testimonial item d-flex flex-row">
								<div class="thumb">
									<img class="img-fluid" src="<?= base_url('assets/') ?>img/elements/user1.png" alt="">
								</div>
								<div class="desc">
									<p>
										Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills, the bigger the payoff you.
									</p>
									<h4>Harriet Maxwell</h4>
									<div class="star">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
							<div class="single-testimonial item d-flex flex-row">
								<div class="thumb">
									<img class="img-fluid" src="<?= base_url('assets/') ?>img/elements/user2.png" alt="">
								</div>
								<div class="desc">
									<p>
										A purpose is the eternal condition for success. Every former smoker can tell you just how hard it is to stop smoking cigarettes. However.
									</p>
									<h4>Carolyn Craig</h4>
									<div class="star">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
							<div class="single-testimonial item d-flex flex-row">
								<div class="thumb">
									<img class="img-fluid" src="<?= base_url('assets/') ?>img/elements/user1.png" alt="">
								</div>
								<div class="desc">
									<p>
										Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills, the bigger the payoff you.
									</p>
									<h4>Harriet Maxwell</h4>
									<div class="star">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
							<div class="single-testimonial item d-flex flex-row">
								<div class="thumb">
									<img class="img-fluid" src="<?= base_url('assets/') ?>img/elements/user2.png" alt="">
								</div>
								<div class="desc">
									<p>
										A purpose is the eternal condition for success. Every former smoker can tell you just how hard it is to stop smoking cigarettes. However.
									</p>
									<h4>Carolyn Craig</h4>
									<div class="star">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End testimonial Area -->