	<!-- start banner Area -->
	<section class="banner-area relative">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-between">
				<div class="col-lg-6 col-md-6 banner-left">
					<h6 class="text-white">let's fly with Us</h6>
					<h1 class="text-white">DREAM AIRLINE</h1>
					<p class="text-white">
						If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for as low as $.17 each.
					</p>
					<a href="<?= base_url('auth') ?>" class="primary-btn text-uppercase">Get Started</a>
				</div>

			</div>
		</div>
	</section>
	<!-- End banner Area -->
	<!-- Start blog Area -->
	<section class="recent-blog-area section-gap">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="menu-content pb-60 col-lg-9">
					<div class="title text-center">
						<h1 class="mb-10">Latest from Our Blog</h1>
						<p>With the exception of Nietzsche, no other madman has contributed so much to human sanity as has.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="active-recent-blog-carusel">
					<div class="single-recent-blog-post item">
						<div class="thumb">
							<img class="img-fluid" src="<?= base_url('assets/') ?>img/b1.jpg" alt="">
						</div>
						<div class="details">
							<div class="tags">
								<ul>
									<li>
										<a href="#">Travel</a>
									</li>
									<li>
										<a href="#">Life Style</a>
									</li>
								</ul>
							</div>
							<a href="#">
								<h4 class="title">Low Cost Advertising</h4>
							</a>
							<p>
								Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
							</p>
							<h6 class="date">31st January,2018</h6>
						</div>
					</div>
					<div class="single-recent-blog-post item">
						<div class="thumb">
							<img class="img-fluid" src="<?= base_url('assets/') ?>img/b2.jpg" alt="">
						</div>
						<div class="details">
							<div class="tags">
								<ul>
									<li>
										<a href="#">Travel</a>
									</li>
									<li>
										<a href="#">Life Style</a>
									</li>
								</ul>
							</div>
							<a href="#">
								<h4 class="title">Creative Outdoor Ads</h4>
							</a>
							<p>
								Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
							</p>
							<h6 class="date">31st January,2018</h6>
						</div>
					</div>
					<div class="single-recent-blog-post item">
						<div class="thumb">
							<img class="img-fluid" src="<?= base_url('assets/') ?>img/b3.jpg" alt="">
						</div>
						<div class="details">
							<div class="tags">
								<ul>
									<li>
										<a href="#">Travel</a>
									</li>
									<li>
										<a href="#">Life Style</a>
									</li>
								</ul>
							</div>
							<a href="#">
								<h4 class="title">It's Classified How To Utilize Free</h4>
							</a>
							<p>
								Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
							</p>
							<h6 class="date">31st January,2018</h6>
						</div>
					</div>
					<div class="single-recent-blog-post item">
						<div class="thumb">
							<img class="img-fluid" src="<?= base_url('assets/') ?>img/b1.jpg" alt="">
						</div>
						<div class="details">
							<div class="tags">
								<ul>
									<li>
										<a href="#">Travel</a>
									</li>
									<li>
										<a href="#">Life Style</a>
									</li>
								</ul>
							</div>
							<a href="#">
								<h4 class="title">Low Cost Advertising</h4>
							</a>
							<p>
								Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
							</p>
							<h6 class="date">31st January,2018</h6>
						</div>
					</div>
					<div class="single-recent-blog-post item">
						<div class="thumb">
							<img class="img-fluid" src="<?= base_url('assets/') ?>img/b2.jpg" alt="">
						</div>
						<div class="details">
							<div class="tags">
								<ul>
									<li>
										<a href="#">Travel</a>
									</li>
									<li>
										<a href="#">Life Style</a>
									</li>
								</ul>
							</div>
							<a href="#">
								<h4 class="title">Creative Outdoor Ads</h4>
							</a>
							<p>
								Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
							</p>
							<h6 class="date">31st January,2018</h6>
						</div>
					</div>
					<div class="single-recent-blog-post item">
						<div class="thumb">
							<img class="img-fluid" src="<?= base_url('assets/') ?>img/b3.jpg" alt="">
						</div>
						<div class="details">
							<div class="tags">
								<ul>
									<li>
										<a href="#">Travel</a>
									</li>
									<li>
										<a href="#">Life Style</a>
									</li>
								</ul>
							</div>
							<a href="#">
								<h4 class="title">It's Classified How To Utilize Free</h4>
							</a>
							<p>
								Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A farmer.
							</p>
							<h6 class="date">31st January,2018</h6>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- End recent-blog Area -->