    <!-- start banner Area -->
    <section class="banner-area relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row fullscreen align-items-center justify-content-between">
                <div class="col-lg-6 col-md-6 mx-auto mt-5">
                    <div class="banner-right mt-5">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="flight" role="tabpanel" aria-labelledby="flight-tab">
                                <?= $this->session->flashdata('message'); ?>
                                <form class="form-wrap" method="post" action="<?= base_url('auth/registration'); ?>">
                                    <h4 class="mb-4">Create a new Account</h4>
                                    <?= form_error('name', '<small class="text-danger">', '</small>'); ?>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Full Name..." value="<?= set_value('email'); ?>">

                                    <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email Address..." value="<?= set_value('email'); ?>">

                                    <?= form_error('password1', '<small class="text-danger">', '</small>'); ?>
                                    <input type="password" class="form-control" id="password1" name="password1" placeholder="New password...">

                                    <input type="password" class="form-control" id="password2" name="password2" placeholder="Re-type new password...">
                                    <p></p>
                                    <input type="submit" class="primary-btn text-uppercase" value="register">
                                </form>
                                <br>
                                <div class="text-center">
                                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                                </div>
                                <br>
                                <div class="text-center">
                                    <a class="small" href="<?php echo base_url('auth/') ?>">Have an account? login here!</a>
                                </div>
                                <br>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End banner Area -->